package com.sumerge.task3.core.mockDB;

import com.sumerge.task3.core.model.Class;
import com.sumerge.task3.core.model.Student;

import java.util.HashMap;
import java.util.Map;

public class DatabaseClass {
    private static Map<Long, Student> students = new HashMap<>();
    private static Map<Long, Class> classes = new HashMap<>();

    public static Map<Long, Student> getStudents(){
        return students;
    }
    public static Map<Long,Class> getClasses(){
        return classes;
    }


    public static void dropDB() {
        students = new HashMap<>();
        classes = new HashMap<>();
    }
}

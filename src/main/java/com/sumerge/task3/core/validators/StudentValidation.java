package com.sumerge.task3.core.validators;

import com.sumerge.task3.core.exception.ObjectDoesNotExistException;
import com.sumerge.task3.core.mockDB.DatabaseClass;
import com.sumerge.task3.core.model.Student;

import java.util.Map;

public class StudentValidation {
    private static Map<java.lang.Long, Student> students = DatabaseClass.getStudents();

    public static boolean studentShouldExist(long id) throws ObjectDoesNotExistException {

        if (!students.containsKey(id))
            throw new ObjectDoesNotExistException("Student with id " + id + " doesn't exist");

        return true;
    }
}

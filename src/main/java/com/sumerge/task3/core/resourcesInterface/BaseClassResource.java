package com.sumerge.task3.core.resourcesInterface;

import com.sumerge.task3.core.model.Class;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/classes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface BaseClassResource {

    @GET
    public List<Class> getClasses(@QueryParam("name") String name,
                                  @QueryParam("studentsIds") List<Long> enrolledStudentsIds);

    @GET
    @Path("/{classId}")
    public Class getClass(@PathParam("classId") long id);

    @POST
    public Class addClass(Class c);

    @POST
    @Path("/{classId}/enrollStudent")
    public Class enrollStudent(@PathParam("classId") long id, @MatrixParam("studentId") long studentId);

    @PUT
    @Path("/{classId}")
    public Class updateClass(@PathParam("classId") long id,Class c);

    @DELETE
    @Path("/{classId}")
    public void deleteClass(@PathParam("classId") long id);


}

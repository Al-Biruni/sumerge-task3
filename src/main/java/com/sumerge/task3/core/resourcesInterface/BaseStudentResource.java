package com.sumerge.task3.core.resourcesInterface;

import com.sumerge.task3.core.model.Student;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/students")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface BaseStudentResource {

    @GET
    public List<Student> getAllStudents(@QueryParam("name") String name,
                                        @QueryParam("enrollmentYear") int enrollmentYear,
                                        @QueryParam("major") String major);

    @POST
    public Student addStudent(Student student);

    @GET
    @Path("/{studentId}")
    public Student getStudent(@PathParam("studentId") long id);

    @PUT
    public Student updateStudent(@PathParam("studentId") long id, Student student);

    @DELETE
    @Path("/{studentId}")
    public void deleteStudent(@PathParam("studentId") long id);
}

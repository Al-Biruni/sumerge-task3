package com.sumerge.task3.core.model;

import java.util.List;
import java.util.Set;

public class Class {
    private long id;
    private String name;
    private Set<Long> studentsIds;

    public Class() {

    }

    public Class(long id, String name, Set<Long> studentsIds) {
        this.id = id;
        this.name = name;
        this.studentsIds = studentsIds;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<Long> getStudentsIds() {
        return studentsIds;
    }

    public void setStudentsIds(Set<Long> studentsIds) {
        this.studentsIds = studentsIds;
    }

    public void enrollStudent(Long studentId) {
        this.studentsIds.add(studentId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

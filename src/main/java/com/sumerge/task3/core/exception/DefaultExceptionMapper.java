package com.sumerge.task3.core.exception;

import com.sumerge.task3.core.model.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DefaultExceptionMapper implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception e) {
        ErrorMessage err = new ErrorMessage(500,e.getMessage());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(err).build();
    }
}


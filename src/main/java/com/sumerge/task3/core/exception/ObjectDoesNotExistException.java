package com.sumerge.task3.core.exception;

public class ObjectDoesNotExistException extends RuntimeException {

    public ObjectDoesNotExistException(String msg) {
        super(msg);

    }

}

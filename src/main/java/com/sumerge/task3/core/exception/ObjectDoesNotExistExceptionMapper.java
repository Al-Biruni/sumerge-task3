package com.sumerge.task3.core.exception;



import com.sumerge.task3.core.model.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ObjectDoesNotExistExceptionMapper implements ExceptionMapper<ObjectDoesNotExistException> {


    @Override
    public Response toResponse(ObjectDoesNotExistException e) {
        ErrorMessage err = new ErrorMessage(400,e.getMessage());
        return Response.status(400).entity(err)
                .build();
    }
}
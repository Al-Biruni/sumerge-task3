package com.sumerge.task3;

import com.sumerge.task3.shell.resources.ClassResource;
import com.sumerge.task3.shell.resources.StudentResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/task3")
public class App extends Application {
    private Set<Object> singletons = new HashSet<>();

    public App() {
//        StudentResource studentResource = new StudentResource();
//        ClassResource classResource = new ClassResource();
//        singletons.add(studentResource);
//        singletons.add(classResource);
    }


    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}

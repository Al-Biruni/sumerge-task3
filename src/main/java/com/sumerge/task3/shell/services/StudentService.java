package com.sumerge.task3.shell.services;

import com.sumerge.task3.core.model.Student;
import com.sumerge.task3.core.mockDB.DatabaseClass;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class StudentService {
    private Map<Long, Student> students = DatabaseClass.getStudents();

    public List<Student> getAllStudents() {
        return new ArrayList<Student>(students.values());
    }

    public Student getStudent(long id){
        return students.get(id);
    }

    public Student addStudent(Student student){
        student.setId(students.size() +1);
        students.put(student.getId(),student);
        return student;

    }
    public Student updateStudent(Student student){
        if(student.getId() <= 0){
            return null;
        }
        students.put(student.getId(),student);
        return  student;
    }
    public Student deleteStudent(long id){
        return students.remove(id);
    }


}

package com.sumerge.task3.shell.services;

import com.sumerge.task3.core.mockDB.DatabaseClass;
import com.sumerge.task3.core.model.Class;
import com.sumerge.task3.core.model.Student;

import java.util.*;

public class ClassService {
    Map<Long, Class> classes = DatabaseClass.getClasses();

    public ClassService(){
        Set<Long> c1StudentsIds = new HashSet<>();
        c1StudentsIds.add(1L);
        c1StudentsIds.add(3L);
        Class c1 = new Class(1,"Drawing101",c1StudentsIds);
        Set<Long> c2StudentsIds = new HashSet<>();
        c2StudentsIds.add(1L);
        c2StudentsIds.add(2L);
        Class c2 = new Class(2,"DataBase",c2StudentsIds);
        classes.put(1L,c1);
        classes.put(2L,c2);
    }

    public List<Class> getClasses() {
        return new ArrayList<Class>(classes.values());
    }

    public Class getClass(long id) {
        return classes.get(id);
    }

    public Class addClass(Class c) {
        c.setId(classes.size() + 1);
        classes.put(c.getId(), c);
        return c;
    }

    public Class updateClass(Class c) {
        if (c.getId() <= 0) {
            return null;
        }
        classes.put(c.getId(), c);
        return c;
    }

    public void removeClass(long id) {
        classes.remove(id);
    }

}

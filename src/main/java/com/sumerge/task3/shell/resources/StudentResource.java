package com.sumerge.task3.shell.resources;

import com.sumerge.task3.core.exception.ObjectDoesNotExistException;
import com.sumerge.task3.core.model.Student;
import com.sumerge.task3.core.resourcesInterface.BaseStudentResource;
import com.sumerge.task3.core.validators.StudentValidation;
import com.sumerge.task3.shell.services.StudentService;

import java.util.List;
import java.util.stream.Collectors;


public class StudentResource implements BaseStudentResource {

    final StudentService studentService;

//    public StudentResource(){
//        studentService = new StudentService();
//    }
    public StudentResource(StudentService studentService){
        this.studentService = studentService;
    }


    public List<Student> getAllStudents(String name, int enrollmentYear , String major){
        List<Student> students = studentService.getAllStudents();
        if(name!=null)
           students = students.stream().filter( student -> student.getName().equals(name)).collect(Collectors.toList());
        if(enrollmentYear !=0)
            students =students.stream().filter(student -> student.getEnrollmentYear() == enrollmentYear).collect(Collectors.toList());
        if(major != null)
            students = students.stream().filter(student -> student.getMajor().equals(major)).collect(Collectors.toList());

        return  students;

    }
    public Student addStudent(Student student){
        return studentService.addStudent(student);
    }


    public Student getStudent(long id){
        Student student =studentService.getStudent(id);
        if(student != null){
            return student;
        }
        throw new ObjectDoesNotExistException("Student with ID "+ id + " doesn't exist");
    }


    public Student updateStudent(long id, Student student) {
        StudentValidation.studentShouldExist(id);
        student.setId(id);
        return studentService.updateStudent(student);
    }


    public void deleteStudent(long id) {
        StudentValidation.studentShouldExist(id);
        studentService.deleteStudent(id);
    }





}

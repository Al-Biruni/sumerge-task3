package com.sumerge.task3.shell.resources;

import com.sumerge.task3.core.exception.ObjectDoesNotExistException;
import com.sumerge.task3.core.model.Class;
import com.sumerge.task3.core.resourcesInterface.BaseClassResource;
import com.sumerge.task3.core.validators.StudentValidation;
import com.sumerge.task3.shell.services.ClassService;

import java.util.List;
import java.util.stream.Collectors;

public class ClassResource implements BaseClassResource {
    ClassService classService = new ClassService();

    public List<Class> getClasses(String name,
                                  List<Long> enrolledStudentsIds) {

        List<Class> classes = classService.getClasses();
        if (name != null)
            classes = classes.stream().filter(c -> c.getName().equals(name)).collect(Collectors.toList());
        if (enrolledStudentsIds.size() > 0) {
            classes = classes.stream().filter(c -> c.getStudentsIds().containsAll(enrolledStudentsIds)).collect(Collectors.toList());
        }

        return classes;
    }


    public Class getClass(long id) {
        Class c= classService.getClass(id);
        if(c !=null)
            return c;
        throw new ObjectDoesNotExistException("Class with ID " + id + " doesn't exist");
    }


    public Class addClass(Class c) {
        return classService.addClass(c);
    }


    public Class enrollStudent(long id, long studentId) {
        Class c = classService.getClass(id);
        StudentValidation.studentShouldExist(studentId);
        c.enrollStudent(studentId);
        return c;
    }


    public Class updateClass(long id, Class c) {
        c.setId(id);
        return classService.updateClass(c);
    }


    public void deleteClass(long id) {
        classService.removeClass(id);

    }

}

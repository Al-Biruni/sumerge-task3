package unitTests;

import com.sumerge.task3.core.exception.ObjectDoesNotExistException;
import com.sumerge.task3.core.mockDB.DatabaseClass;
import com.sumerge.task3.core.model.Student;
import com.sumerge.task3.shell.services.StudentService;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.*;
import com.sumerge.task3.shell.resources.StudentResource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class StudentServiceTest {
    @AfterEach
    void cleanUp(){
        DatabaseClass.dropDB();
    }

    @Test
    @Order(1)
    void getAllStudentsWorks() {
        StudentService testStudentService = new StudentService();
        Student s1 = new Student(1L,"S1",2020,"m1");
        Student s2 = new Student(2L,"S2",2019,"m1");
        Student s3 = new Student(3L,"S3",2018,"m2");
        testStudentService.addStudent(s1);
        testStudentService.addStudent(s2);
        testStudentService.addStudent(s3);
        List<Student> students = testStudentService.getAllStudents();
        assertEquals(3,students.size());
//        students = testResource.getAllStudents("S1",0,null);
//        assertEquals(1,students.size());
//        students = testResource.getAllStudents(null,0,"m1");
//        assertEquals(2,students.size());
//        students = testResource.getAllStudents(null,2020,null);
//        assertEquals(1,students.size());

    }

    @Test
    @Order(2)
    void addStudentWorks() {
        StudentService testStudentService = new StudentService();
        Student s1 = new Student(1L,"S1",2020,"m1");
        testStudentService.addStudent(s1);
        Student responseStudent =  testStudentService.getStudent(1L);
        assertEquals(s1.getId(),responseStudent.getId());
        assertEquals(s1.getName(),responseStudent.getName());
        assertEquals(s1.getEnrollmentYear(),responseStudent.getEnrollmentYear());
        assertEquals(s1.getMajor(),responseStudent.getMajor());

    }

    @Test
    @Order(3)
    void getStudentWorks() {
        StudentService testStudentService = new StudentService();
        Student s1 = new Student(1L,"S1",2020,"m1");
        testStudentService.addStudent(s1);
        Student responseStudent =  testStudentService.getStudent(1L);
        assertEquals(s1.getId(),responseStudent.getId());
        assertEquals(s1.getName(),responseStudent.getName());
        assertEquals(s1.getEnrollmentYear(),responseStudent.getEnrollmentYear());
        assertEquals(s1.getMajor(),responseStudent.getMajor());
    }


    @Test
    @Order(4)
    void updateStudentWorks() {
        StudentService testStudentService = new StudentService();

        Student s1 = new Student(1L,"S1",2020,"m1");
        testStudentService.addStudent(s1);
        Student updatedStudent = new Student(1L,"updatedName",2020,"updatedMajor");

        testStudentService.updateStudent(updatedStudent);
        Student responseStudent =  testStudentService.getStudent(1L);
        assertEquals(updatedStudent.getId(),responseStudent.getId());
        assertEquals(updatedStudent.getName(),responseStudent.getName());
        assertEquals(updatedStudent.getEnrollmentYear(),responseStudent.getEnrollmentYear());
        assertEquals(updatedStudent.getMajor(),responseStudent.getMajor());
    }

    @Test
    @Order(5)
    void deleteStudentWorks() {
        StudentService testStudentService = new StudentService();
        Student s1 = new Student(1L,"S1",2020,"m1");
        testStudentService.addStudent(s1);
        List<Student> students =  testStudentService.getAllStudents();
        assertEquals(1,students.size());

        testStudentService.deleteStudent(s1.getId());
        students =  testStudentService.getAllStudents();

        assertFalse(students.contains(s1));


    }

//    @Test
//    @Order(6)
//    void getStudentWithInvalidIdThrows() {
//        StudentService testStudentService = new StudentService();
//
//        try {
//         testResource.getStudent(1878L);
//        }catch (ObjectDoesNotExistException e){
//            return;
//        }
//        fail("Student with invalid id method should throw object doesn't exist exception");
//
//
//    }
}
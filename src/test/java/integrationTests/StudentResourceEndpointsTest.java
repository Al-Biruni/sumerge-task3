package integrationTests;

import com.sumerge.task3.core.model.Student;
import com.sumerge.task3.shell.services.StudentService;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

@QuarkusTest
public class StudentResourceEndpointsTest {


    @InjectMock
    StudentService studentService;


    @Test
    public void testGetStudentsEndpoint() {
        when(studentService.getAllStudents()).thenReturn(Arrays.asList(
                new Student(1L, "S1", 2020, "M1")));
        given()
                .when().get("/task3/students")
                .then()
                .statusCode(200)
                .body(is("[{\"id\":1,\"name\":\"S1\",\"enrollmentYear\":2020,\"major\":\"M1\"}]"));
    }

    @Test
    public void testGetStudentEndpointWorks() {
        Student testStudent = new Student(1L, "S1", 2020, "M1");
        when(studentService.getStudent(1)).thenReturn( testStudent);

        given()
                .when().get("/task3/students/1")
                .then()
                .statusCode(200)
                .body(is("{\"id\":1,\"name\":\"S1\",\"enrollmentYear\":2020,\"major\":\"M1\"}"));

    }

    @Test
    public void testGetStudentWithInvalidIdThrows() {

        given()
                .when().get("/task3/students/1")
                .then()
                .statusCode(400)
                .body(is("{\"statusCode\":400,\"message\":\"Student with ID 1 doesn't exist\"}"));

    }

}




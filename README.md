
## Application Paths

### - Student resources

```
@Get /task3/students    queryParams{ name | enrollmentYear | major }
```
```
@Post /task3/students/   
```
```
@Get /task3/students/{studentID}
```
```
@Put /task3/students/{studentID}
```
```
@Delete /task3/students/{studentID}
```

### Classes resources

```
@Get /task3/classes    queryParams{ name | studentsIds[] }
```
```
@Post /task3/classes/{classId}/enrollStudent;studentId={id}   
```
```
@Post /task3/classes/   
```
```
@Get /task3/classes /{classId}
```
```
@Put /task3/classes/{classId}
```
```
@Delete /task3/classes/{classId}
```


## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw quarkus:dev
```